# Vitrine How-To Guides

## How to customize my Vitrine website?

The customization of Vitrine will seem most logical to users already familiar with Discourse (and/or Gitlab).

**Discourse** is a typical forum software which organizes its content in `topics` and `categories`. The equivalent for **Gitlab** would be `projects` (Git repositories) and `groups`, but we'll use Discourse to explain the way Vitrine works.

In Vitrine, everything either comes from a Discourse category or a topic, and everything is displayed in `pages` and `zones` (which are themselves different parts of a page).

The config file is written in the YAML format. You must remember that indentations are what structures this file, they are important to understand its hierarchy and the way to write your configuration.

## Quick start

In the following example, we have configured a "Projects" page with a "Civic Tech" zone which takes its content from a Discourse category which is identified by a number. This is the ID of the category you'll find at the end of its URL.

```yaml
pages:
  - slug: projects
    zones:
      - title: Civic Tech
        source:
          type: discourse
          url: https://my.discourse.example
          category: 13
```

## Glossary

- category: a folder on Discourse which contains topics. Identified with an ID.
- group: a folder on Gitlab which contains projects.
- page: a single page on Vitrine which contains zones. Defined with a slug and optionnaly a title.
- project: a repository on Gitlab which contains files.
- source: used to define the source of a zone.
- slug: the string of text displayed in a URL and which usually corresponds to a page on the website.
- topic: a page on Discourse which contains posts. Vitrine retrieves the content of the first post.
- zone: a block of content on a Vitrine page which contains displaying content, either from a Discourse topic, a GitLab project and another type of sources.
