# Vitrine

Vitrine (French for "showcase") is a free and open source tool to easily generate a website using content retrieved from third-party pages.
Originally, its main use case was for a hackathon landing page, to showcase its projects and participants, datasets, tools, etc.
Vitrine can also be used for any other purpose.

## Current instances

- 2021 [Résilience des territoires Call for projects](https://resilience-territoire.ademe.fr/) ([repo](https://gitlab.com/jailbreak/vitrine-resilience-territoires))
- 2020 [DataBât Hackathon](https://databat.ademe.fr/) ([repo](https://gitlab.com/jailbreak/vitrine-databat/))
- 2019 [AGDIC Hackathon](https://agdic-hackathon.frama.io/) ([repo](https://framagit.org/agdic-hackathon/agdic-hackathon.frama.io))
- 2018 & 2020 [DataFin Hackathon](https://datafin.fr/) ([repo](https://git.en-root.org/eraviart/vitrine/))

## Features

### Discourse and Gitlab as a CMS

Vitrine was inspired by other **content managing systems (CMS)** such as [Hackdash](https://www.hackdash.org/) or [Wordpress](https://www.wordpress.org) but instead of having its own back office / admin panel, it's based on [Discourse](https://www.discourse.org/) and [GitLab](https://about.gitlab.com/). This allows you to take advantage of the rich features offered by these tools, especially for community management, while at the same time using them as a CMS to edit collaboratively and in near-real time the content displayed in Vitrine.
