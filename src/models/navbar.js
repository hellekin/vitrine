import { ArrayModel, ObjectModel } from "objectmodel"

const NavbarButtons = new ObjectModel({
	title: String,
	url: String,
})

export default new ObjectModel({
	title: [String],
	logo: [String],
	textColor: [String],
	hoverColor: [String],
	externalLinks: [ArrayModel(NavbarButtons)],
}).as("Navbar")
