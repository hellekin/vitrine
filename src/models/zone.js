import { ObjectModel } from "objectmodel"

import { SourceDiscourse, SourceGitlab, SourceHtml, SourceMarkdown } from "./source"
import { ButtonWithPageSlug, ButtonWithUrl } from "./button"

export default new ObjectModel({
	title: [String],
	slug: [String],
	source: [SourceDiscourse, SourceGitlab, SourceHtml, SourceMarkdown],
	backgroundImage: [String],
	backgroundColor: [String],
	button: [ButtonWithPageSlug, ButtonWithUrl, undefined], // https://objectmodel.js.org/#doc-multiple-types
}).as("Zone")
