import OError from "@overleaf/o-error"
import { ConfigError, FetchError } from "../fetchers/errors"
import Config from "../models/config.js"

export async function getPageProps(pageSlug, configData, context) {
	const config = new Config(configData)
	const page = config.findPageBySlug(pageSlug)

	let data
	try {
		data = await page.fetchData(config, context)
	} catch (e) {
		if (e instanceof ConfigError || e instanceof FetchError) {
			console.error(OError.getFullStack(e))
			console.error(OError.getFullInfo(e))

			return context.error(500, e.message)
		}
		throw e
	}

	return { configData, pageSlug, data }
}
