=   : exact value
!   : mandatory field
xor : just one of the field should have a value

=version: 3
title: Vitrine website title
navbar:
  title: a title
  logo: file.jpeg
  textColor: "#000000"
  hoverColor: "#1e1e1e"
  externalLinks:
    - !title: A button
      !url: https://twitter.com
filterTag: hackathon-juin-2018
pages:
  - !slug: ""
    navbarTitle: a text (if present , display page navbarTitle in menu)
    banner:
      classes: w-full max-w-6xl mx-auto
      url:
    title: a title
    subtitle: a subtitle
    callToActionButton:
      title: Inscrivez-vous
      url: https://www.jailbreak.paris
    zones:
      - title: a title
        slug: an-anchor
        backgroundImage: a_file.html
        !source: SOURCE
        button: BUTTON

SOURCE: (SOURCE_DISCOURSE xor SOURCE_GITLAB xor SOURCE_HTML xor SOURCE_MARKDOWN)

SOURCE_DISCOURSE: (category xor topic)
  =type: discourse
  !url: https://forum.datafin.fr
  category: 14
  #topic: 366
  limit: 4
  cardsPerLine: 3
  cardsPerLineSm: 2
  filterTag: winning_projects
  disableBlur: true
  disableReadMore: true

SOURCE_GITLAB:
  =type: gitlab
  !url: https://framagit.org
  !group: agdic-hackathon/challenges
  limit: 4
  cardsPerLine: 3
  cardsPerLineSm: 2
  disableBlur: true
  disableReadMore: true

SOURCE_HTML: (file xor content)
  =type: html
  raw: true
  file: file.html
  content: |
    <div>some html</div>

SOURCE_MARKDOWN:
  =type: markdown
  !file: file.md

BUTTON: (BUTTON_WITH_PAGE_SLUG xor BUTTON_WITH_URL)

BUTTON_WITH_PAGE_SLUG:
  !title: Inscrivez-vous
  !slug: defis

BUTTON_WITH_URL:
  !title: Inscrivez-vous
  !url: https://senat.limequery.org/759236?newtest=Y&lang=fr
